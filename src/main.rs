

fn main() {
    println!("Hello, world!");
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Direction {
    N, E, S, W
}

#[derive(PartialEq)]
pub enum Command {
    R, L, M
}

impl Direction {
    pub fn right(&self) -> Self {
        match self {
            Direction::N => Direction::E,
            Direction::E => Direction::S,
            Direction::S => Direction::W,
            Direction::W => Direction::N
        }
    } 

    pub fn left(&self) -> Self {
        match self {
            Direction::N => Direction::W,
            Direction::W => Direction::S,
            Direction::S => Direction::E,
            Direction::E => Direction::N
        }
    } 
}

#[derive(Debug)]
pub struct MarsRover {
    x: u32,
    y: u32,
    direction: Direction
}

impl MarsRover {
    pub fn new(x: u32, y: u32, direction: Direction) -> Self {
        MarsRover{x:x, y:y, direction: direction}
    }
}

pub struct Grid {}

impl Grid {
    pub fn new() -> Self {
        Grid{}
    }
}

fn turn_right(rover: &MarsRover) -> MarsRover {
    MarsRover::new(rover.x, rover.y, rover.direction.right())
}

fn turn_left(rover: &MarsRover) -> MarsRover {
    MarsRover::new(rover.x, rover.y, rover.direction.left())
}

pub fn execute_command(mars_rover: MarsRover, commands: &str) -> Result<String, &str> { 
    let mut rover = mars_rover;
    for command in commands.chars() {
        rover = match command {
            'R' => turn_right(&rover),
            'L' => turn_left(&rover),
            _ => rover
        }  
    }
    Ok(format!("{}:{}:{:?}", rover.x, rover.y, rover.direction))
}

#[cfg(test)]
mod mars_rover_tests {
    use super::*;

    #[test]
    fn when_input_is_blank_verify_no_change_in_position() {
        let mars_rover = MarsRover::new(0, 0, Direction::N);
        let result = execute_command(mars_rover, "");
        assert_eq!(Ok(String::from("0:0:N")), result);
    }

    #[test]
    fn when_input_is_invalid_verify_no_change_in_position() {
        let mars_rover = MarsRover::new(0, 0, Direction::N);
        let result = execute_command(mars_rover, "A");
        assert_eq!(Ok(String::from("0:0:N")), result);
    }

    #[test]
    fn when_input_right_command_verify_facing_east() {
        let mars_rover = MarsRover::new(0, 0, Direction::N);
        let result = execute_command(mars_rover, "R");
        assert_eq!(Ok(String::from("0:0:E")), result);
    }

    #[test]
    fn when_input_right_twice_command_verify_facing_south() {
        let mars_rover = MarsRover::new(0, 0, Direction::N);
        let result = execute_command(mars_rover, "RR");
        assert_eq!(Ok(String::from("0:0:S")), result);
    }

    #[test]
    fn when_input_left_three_times_command_verify_facing_east() {
        let mars_rover = MarsRover::new(0, 0, Direction::N);
        let result = execute_command(mars_rover, "LLL");
        assert_eq!(Ok(String::from("0:0:E")), result);
    }
}